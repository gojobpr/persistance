package persistance

import (
	"context"
	"errors"
	"fmt"
	"github.com/rs/zerolog"
	gormL "gorm.io/gorm/logger"
	"time"

	"gitlab.com/gojobpr/logger"
)

type LogLevel string

const (
	Silent LogLevel = "Silent"
	Error  LogLevel = "Error"
	Warn   LogLevel = "Warn"
	Info   LogLevel = "Info"
)

const defaultSlowThreshold = 200 * time.Millisecond
const defaultLevel = Warn

func (lL LogLevel) validate() {
	switch lL {
	case Silent, Error, Warn, Info:
		return
	}

	logger.Log.Panic().Msg(fmt.Sprintf("invalid databae log level [%v] provided", lL))
	panic(fmt.Sprintf("invalid databae log level [%v] provided", lL))
}

func (lL LogLevel) Int() int {
	switch lL {
	case Silent:
		return 1
	case Error:
		return 2
	case Warn:
		return 3
	case Info:
		return 4
	}
	return 0
}

func newLogger(SlowThreshold time.Duration, level LogLevel) gormLogger {
	lI := gormLogger{SlowThreshold, true, level}

	return lI
}

type gormLogger struct {
	SlowThreshold             time.Duration
	IgnoreRecordNotFoundError bool
	LogLevel                  LogLevel
}

func (l gormLogger) LogMode(gormL.LogLevel) gormL.Interface {
	return l
}

func (l gormLogger) Error(_ context.Context, msg string, opts ...interface{}) {
	logger.Log.Error().Msg(fmt.Sprintf(msg, opts...))
}

func (l gormLogger) Warn(_ context.Context, msg string, opts ...interface{}) {
	logger.Log.Warn().Msg(fmt.Sprintf(msg, opts...))
}

func (l gormLogger) Info(_ context.Context, msg string, opts ...interface{}) {
	logger.Log.Info().Msg(fmt.Sprintf(msg, opts...))
}

func (l gormLogger) Trace(_ context.Context, begin time.Time, f func() (string, int64), err error) {
	elapsed := time.Since(begin)
	switch {
	case err != nil && l.LogLevel >= Error && (!errors.Is(err, gormL.ErrRecordNotFound) || !l.IgnoreRecordNotFoundError):
		event := l.buildTracedEvent(err, elapsed, f)
		event.Err(err)
		event.Send()
	case elapsed > l.SlowThreshold && l.SlowThreshold != 0 && l.LogLevel >= Warn:
		event := l.buildTracedEvent(err, elapsed, f)
		event.Str("SLOW QUERY", fmt.Sprintf("duration >= %v", l.SlowThreshold))
		event.Send()
	case l.LogLevel == Info:
		event := l.buildTracedEvent(err, elapsed, f)
		event.Send()
	}
}

func (l gormLogger) buildTracedEvent(err error, elapsed time.Duration, f func() (string, int64)) *zerolog.Event {
	var event *zerolog.Event

	sql, rows := f()

	if err != nil {
		event = logger.Log.Debug()
	} else {
		event = logger.Log.Trace()
	}

	event.Dur(l.getDurationUnits(), elapsed)
	if sql != "" {
		event.Str("sql", sql)
	}
	if rows > -1 {
		event.Int64("rows", rows)
	}

	return event
}

func (l gormLogger) getDurationUnits() string {
	var durKey string

	switch zerolog.DurationFieldUnit {
	case time.Nanosecond:
		durKey = "elapsed_ns"
	case time.Microsecond:
		durKey = "elapsed_us"
	case time.Millisecond:
		durKey = "elapsed_ms"
	case time.Second:
		durKey = "elapsed"
	case time.Minute:
		durKey = "elapsed_min"
	case time.Hour:
		durKey = "elapsed_hr"
	default:
		logger.Log.Error().Interface(
			"zerolog.DurationFieldUnit",
			zerolog.DurationFieldUnit,
		).Msg("gormzerolog encountered a mysterious, unknown value for DurationFieldUnit")
		durKey = "elapsed_"
	}

	return durKey
}
