package persistance

import (
	"fmt"
	"reflect"
	"time"

	"gitlab.com/gojobpr/logger"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Postgres struct {
	*gorm.DB
}

type PostgresConfig struct {
	DSN           string
	SlowThreshold time.Duration
	LogLevel      LogLevel
}

func (c *PostgresConfig) validate() {
	if uint64(c.SlowThreshold) == 0 {
		c.SlowThreshold = defaultSlowThreshold
	}

	if c.LogLevel == "" {
		c.LogLevel = defaultLevel
	}
	c.LogLevel.validate()

	if c.DSN == "" {
		logger.Log.Panic().Msg("Postgres connection string not provided")
		panic("Postgres connection string not provided")
	}
}

func pGConnect(c PostgresConfig) *Postgres {
	c.validate()

	db, dbError := gorm.Open(postgres.Open(c.DSN), &gorm.Config{Logger: newLogger(c.SlowThreshold, c.LogLevel)})
	if dbError != nil {
		logger.Log.Panic().Err(dbError).Msg("Cannot connect to DB")
		panic(dbError)
	}

	return &Postgres{db}
}

func (pg Postgres) AutoMigrate(models ...interface{}) {
	for _, m := range models {
		isModel := false
		t := reflect.TypeOf(m)
		for i := 0; i < t.NumField(); i++ {
			if t.Field(i).Type.Name() == reflect.TypeOf(gorm.Model{}).Name() {
				isModel = true
				err := pg.DB.AutoMigrate(&m)
				if err != nil {
					logger.Log.Panic().Err(err).Str("model", fmt.Sprintf("%T", m)).Msg("Cannot migrate")
					panic(fmt.Sprintf("Cannot migrate %T", m))
				}

				break
			}
		}

		if isModel == false {
			logger.Log.Warn().Str("value", t.Name()).Msg("Is not valid model to migrate, ignored")
		}
	}
}
