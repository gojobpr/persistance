package persistance

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestNotProvidedDSN(t *testing.T) {
	c := PostgresConfig{}

	isPanic := testPanic(func() { c.validate() })

	assert.Equal(t, true, isPanic)
}

func TestConnectConfigInvalidLevel(t *testing.T) {
	c := PostgresConfig{"DSN", 100 * time.Microsecond, "Invalid"}

	isPanic := testPanic(func() { c.validate() })

	assert.Equal(t, true, isPanic)
}

func TestFullConfig(t *testing.T) {
	c := PostgresConfig{"DSN", 100 * time.Millisecond, Warn}

	isPanic := testPanic(func() { c.validate() })

	assert.Equal(t, false, isPanic)
}

func TestNotUseOptionalParameters(t *testing.T) {
	c := PostgresConfig{DSN: "DSN"}

	isPanic := testPanic(func() { c.validate() })

	assert.Equal(t, false, isPanic)
	assert.Equal(t, defaultSlowThreshold, c.SlowThreshold)
	assert.Equal(t, defaultLevel, c.LogLevel)
}

func testPanic(testFunc func()) (isPanic bool) {
	defer func() {
		if err := recover(); err != nil {
			isPanic = true
		}
	}()
	testFunc()

	return false
}
