# Persistance

Abstraction layer on top of different persistence storages which providing interface to easily configure and connect to different storages.

Currently supporting:
- postgres - under the hood using https://gorm.io/
- 
## Usage

```go
package main

import (
  "gitlab.com/gojobpr/persistance"
  "time"
)

func main() {
  persistance.PostgresConnect(
    persistance.PostgresConfig{
      "host=project_db user=postgres password=postgres dbname=postgres port=5432 sslmode=disable",
      200 * time.Millisecond,
      persistance.Warn,
    },
  )

  result := map[string]interface{}{}
  persistance.PG.DB.Model(&User{}).First(&result)

  // or you can omit most of the configurations and default values will be used. Default values: 
  //for SlowThreshold is 200ms, and Warn log level as a LogLevel
  persistance.PostgresConnect(
    persistance.PostgresConfig{
      "host=project_db user=postgres password=postgres dbname=postgres port=5432 sslmode=disable",
    },
  )

  result = map[string]interface{}{}
  persistance.PG.DB.Model(&User{}).First(&result)

  // also you can automatically migrate your models
  persistance.PG.AutoMigrate(User{})
}
```

## API

---
#### `PostgresConnect(c PostgresConfig)` - Initialize postgres connection

### PostgresConfig
#### `PostgresConfig` - struct which is used to configure postgres connection.
Structure:
```go
type PostgresConfig struct {
  DSN           string
  SlowThreshold time.Duration
  LogLevel      LogLevel
}
```
Fields:
- `DSN string` - postgres connection string:
- `SlowThreshold time.Duration` - what is maximum duration which is not considered as a slow query for application, everything above that wil lbe logged
- `LogLevel LogLevel` - which logs level related to database should be logged
   -  Silent LogLevel | "Silent" string
   -  Error  LogLevel | "Error" string
   -  Warn   LogLevel | "Warn" string
   -  Info   LogLevel | "Info" string

---

## TODO

- [ ] supported of other databases if will be required, for example redis, MySql, MongoDB etc.
- [ ] Ability to have multiple connection of the same type
